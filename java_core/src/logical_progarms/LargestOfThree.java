package logical_progarms;
import java.util.Scanner;
public class LargestOfThree {

	public static void main(String[] args)
	{
		int result=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the first number: ");
		int num1=sc.nextInt();
		System.out.println("enter the second number: ");
		int num2=sc.nextInt();
		System.out.println("enter the third number: ");
		int num3=sc.nextInt();
		
		result=(num1>num2?(num1>num3?num1:(num2>num3?num2:num3)):(num2>num3?num2:num3));
		System.out.println(result +"is greater number");
	}

}
