package object_classes;

public class BookDriver {

	public static void main(String[] args) 
	{
		Book b1=new Book("java");
		Book b2=new Book("java");
		System.out.println(b1==b2);
		System.out.println(b1.equals(b2));
	}

}
