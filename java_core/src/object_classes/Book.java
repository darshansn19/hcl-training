package object_classes;

public class Book 
{
	String bookName;
	Book(String bookName)
	{
		this.bookName=bookName;
	}
	public boolean equals(Object o)
	{
		Book b=(Book)o;
		return this.bookName==b.bookName;
			
	}
}
