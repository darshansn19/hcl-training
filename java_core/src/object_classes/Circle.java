package object_classes;

public class Circle 
{
	int radius;
	Circle(int radius)
	{
		this.radius=radius;
	}
	
	public String toString()
	{
		return "radius: "+radius;
	}
	public static void main(String[] agrs)
	{
		Circle c=new Circle(5);
		System.out.println(c);
		
	}
}
