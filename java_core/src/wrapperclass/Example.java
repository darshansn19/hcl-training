package wrapperclass;

public class Example {

	public static void main(String[] args) 
	{
		//primitive to object: "boxing"
		int a=10;
		System.out.println("before wrapping "+ a);
		
		Integer i=Integer.valueOf(a);
		System.out.println("after wrapping...."+i);
		
		char c='a';
		Character ch=Character.valueOf(c);
		System.out.println("after wrapping "+ch);
		
		float f=2.2f;
		Float b=Float.valueOf(f);
		System.out.println("after wrapping "+b);
		
		
		//object to primitive: "unboxing"
		
		int in=i.intValue();
		System.out.println(in);

	}

}
