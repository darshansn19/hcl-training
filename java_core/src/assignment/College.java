package assignment;
import java.util.Scanner;
public class College
{
	String cname;
	Department d;
	Scanner sc=new Scanner(System.in);
	College(String cname,Department d)
	{
		this.cname=cname;
		this.d=d;
	}
	 public  void addDepartment()
	 {
		 if(d==null)
		 {
			 System.out.println("select the department: ");
			 System.out.println("we have....\n1.ECE\n2.CSE\n3.ISE");
			 System.out.println("enter your choice: ");
			 int choice=sc.nextInt();
			 if(choice==1)
				{
					this.d=new Ece("ece",10,10000);
					System.out.println("ece department is added ");
				}
			 else if(choice==2)
			 {
				 this.d=new Cse("cse",20,20000);
					System.out.println(" cse department is added ");
			 }
			 else
			 {
				 this.d=new Ise("ise",30,30000);
					System.out.println(" ise department is added ");
			 }
		 }
		 else
			 System.out.println("department is already created");
	 }
	 
	 public void removeDepartment()
	 {
		 if(d!=null)
		 {
			 if(d instanceof Ece)
			 {
			 this.d=null;
			 System.out.println(" Ece department is removed successfully");
			 }
			 else if(d instanceof Cse)
			 {
				 this.d=null;
				 System.out.println(" Cse department is removed successfully");
				 
			 }
			 else
			 {
				 this.d=null;
				 System.out.println(" Ise department is removed successfully");
				 
			 }
		 }
		 
		 else
			 System.out.println("add department to college first");
	 }
	 
	public void deptDetails()
	{
		if(d!=null)
		{
		
			
			d.departmentDetails();
			System.out.println("college name "+cname);
			if(d instanceof Ece)
			{
				Ece e=(Ece)d;
				e.cfee();
			}
			else if(d instanceof Cse)
			{
				Cse a=(Cse)d;
				a.cfee();
			}
			else
			{
				Ise i=(Ise)d;
				i.cfee();
			}
				
		}
		else
			System.out.println("creat a department first");
	}
}
