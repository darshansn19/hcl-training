package assignment;
import java.util.Scanner;
public class HostelDriver {

	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the hostel name");
		String hname=sc.next();
		Hostel h=new Hostel(hname,null);
		boolean control=true;
		while(control)
		{
			System.out.println("1.addstudent\n2.remove student\n3.update phoneno\n4.student details\n5.exit");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
			{
				h.addstudent();
				break;
			}
			case 2:
			{
				h.removeStudent();
				break;
			}
			case 3:
			{
				h.setphone();
				break;
			}
			case 4:
			{
				h.studentDetails();
				System.out.println("hstel name: "+hname);
				break;
			}
			case 5:
			{
				control=false;
				System.out.println("service is closed");
			}
			}
		}

	}

}
