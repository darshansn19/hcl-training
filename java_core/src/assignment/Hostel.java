package assignment;
import java.util.Scanner;
public class Hostel
{
	String hname;
	Student s;
	Hostel(String hname,Student s)
	{
		this.hname=hname;
		this.s=s;
	}
	Scanner sc=new Scanner(System.in);
	public void addstudent()
	{
		if(s==null)
		{
			System.out.println("enter the student number:");
			int sno=sc.nextInt();
			System.out.println("enter the student name: ");
			String sname=sc.next();
			System.out.println("enter the age: ");
			int age=sc.nextInt();
			System.out.println("enter the phone number: ");
			long phone=sc.nextLong();
			this.s=new Student(sno,sname,age,phone);
			System.out.println("Student admission to the hostel class successfull");	
		}
		else
			System.out.println("student already exist");
	}
	public void removeStudent()
	{
		if(s!=null)
		{
			this.s=null;
			System.out.println("student is removed succesfully");	
		}
		else
			System.out.println("hostel is already empty");
	}
	
	public void setphone()
	{
		if(s!=null)
		{
			System.out.println("enter the phone number");
			long phone=sc.nextLong();
			s.setphone(phone);	
		}
		else
			System.out.println("admit the student first");
	}
	
	public void studentDetails()
	{
		if(s!=null)
		{
			s.displayDetails();
		}
		else
			System.out.println("admit the student first");
	}

}
