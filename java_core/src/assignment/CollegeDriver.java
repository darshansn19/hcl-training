package assignment;
import java.util.Scanner;
public class CollegeDriver {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		boolean control=true;
		System.out.println("enter the college name ");
		String cname=sc.next();
		College c=new College(cname,null);
		while(control)
		{
			System.out.println("1.add department\n2.remove department\n3.department details\n4.exit");
			System.out.println("enter the choice");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
			{
				c.addDepartment();
				break;
			}
			case 2:
			{
				c.removeDepartment();
				break;
			}
			case 3:
			{ 	
				c.deptDetails();
				
				break;
			}
			case 4:
			{
				control=false;
				System.out.println("colleged is closed");
			}
			}
		}

	}

}
