package assignment;

public class Student 
{
	private int sno;
	private String sname;
	private int age;
	private long phone;
	Student(int sno,String sname,int age,long phone)
	{
		this.sno=sno;
		this.sname=sname;
		this.age=age;
		this.phone=phone;
	}
	public int getsno()
	{
		return sno;
	}
	public String getsname()
	{
		return sname;
	}
	public int getage()
	{
		return age;
	}
	public long getphone()
	{
		return phone;
	}
	public void setphone(long newphone)
	{
		this.phone=newphone;
	}
	public void displayDetails()
	{
		System.out.println("student number is :"+getsno());
		System.out.println("student name is :"+getsname());
		System.out.println("student age is :"+getage());
		System.out.println("student phone is:"+getphone());
		
	}

}
