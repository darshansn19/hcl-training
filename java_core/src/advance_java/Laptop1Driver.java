package advance_java;

public class Laptop1Driver {

	public static void main(String[] args)
	{
		Laptop1 l1=getLaptop();
		Laptop1 l2=getLaptop();

		l1.setRam(4);
		l1.setHd(1);
		l1.setPrice(33000);
		l1.setBrand("hp");
		
		l2.setRam(4);
		l2.setHd(1);
		l2.setPrice(44000);
		l2.setBrand("dell");
		System.out.println("laptop 1 details");
		
		Laptop1Driver.printDetais(l1);
		Laptop1Driver.printDetais(l2);
		
		
	}

	public static Laptop1 getLaptop() {
		Laptop1 obj=new Laptop1();
		return obj;
	}
	
	public static void printDetais(Laptop1 laptop1)
	{
		System.out.println(laptop1.getRam());
		System.out.println(laptop1.getHd());
		System.out.println(laptop1.getPrice());
		System.out.println(laptop1.getBrand());;
	}

	
}
