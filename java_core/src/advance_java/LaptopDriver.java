package advance_java;

public class LaptopDriver {

	public static void main(String[] args) 
	{
		
		Laptop l1=getLaptop();
		Laptop l2=getLaptop();
	
			l1.ram=4;
			l1.hd=1;
			l1.price=33000;
			l1.brand="hp";
			
			l2.ram=8;
			l2.hd=1;
			l2.price=45000;
			l2.brand="dell";
			
			
		System.out.println("laptop 1 details ");
		LaptopDriver.printDetails(l1);
//		System.out.println(l1.ram);
//		System.out.println(l1.hd);
//		System.out.println(l1.price);
//		System.out.println(l1.brand);
		
		System.out.println("laptop 2 details ");
		LaptopDriver.printDetails(l2);
//		System.out.println(l2.ram);
//		System.out.println(l2.hd);
//		System.out.println(l2.price);
//		System.out.println(l2.brand);
	}
	
	public static Laptop getLaptop()//method to return the reference
	{
		Laptop obj=new Laptop();
		return obj;
		//return new Laptop();
	}
	
	public static void printDetails(Laptop laptop)//method to accept the reference
	{
		System.out.println(laptop.ram);
		System.out.println(laptop.hd);
		System.out.println(laptop.price);
		System.out.println(laptop.brand);
		
		
		
	}
	

}
