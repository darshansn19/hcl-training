package advance_java;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		Employee e1=EmployeeDriver.getEmployee();
		Employee e2=EmployeeDriver.getEmployee();
		e1.setEid(27);
		e1.setEname("darshan");
		e1.setAge(20);
		e1.setSalary(50000);
		e1.setDesig("developer");
		
		e2.setEid(28);
		e2.setEname("sarathi");
		e2.setAge(21);
		e2.setSalary(60000);
		e2.setDesig("tester");
		System.out.println("employee 1 details");
		EmployeeDriver.printEmployeeDetails(e1);

		System.out.println("employee 2 details");
		EmployeeDriver.printEmployeeDetails(e2);
		

	}
	
	public static Employee getEmployee()
	{
		Employee obj=new Employee();
		return obj;
	}
	public static void printEmployeeDetails(Employee em)
	{
		System.out.println(em.getEid());
		System.out.println(em.getEname());
		System.out.println(em.getAge());
		System.out.println(em.getSalary());
		System.out.println(em.getDesig());
	}

}
