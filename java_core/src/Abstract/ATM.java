package Abstract;

public   abstract class ATM 
{
	public abstract void withdraw();
	public abstract void deposit();
	 
	public  void display()
	{
		System.out.println("non static method for class..");
	}
	public static void test()
	{
		System.out.println("static method for class...");
	}
}
