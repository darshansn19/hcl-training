package encapsulation;

public class Student
{
	
	private int sid;
	private String sname;
	private int age;
	
	Student(int sid,String sname, int age)
	{
		this.sid=sid;
		this.sname=sname;
		this.age=age;
	}
	
	//getter method for sid
	public int getid()
	{
		return sid;
	}
	//setter method for sid
	public void setid(int sid)
	{
		this.sid=sid;
	}
	public String getname()

	{
		return sname;
	}
	public void setname(String sname)
	{
		this.sname=sname;
	}
	
	public int getage()
	{
		return age;
	}
	
	public void setage(int age)
	{
		this.age=age;
	}
	
	
	
}