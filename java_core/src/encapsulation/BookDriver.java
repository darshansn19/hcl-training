package encapsulation;

import java.util.Scanner;

public class BookDriver 
{ 
	public static void main(String[] args) 
	
	{
		
		Scanner sc=new Scanner(System.in);
		System.out.print("enter book id :");
		int bookid=sc.nextInt();
		System.out.print("enter book title :");
		String title=sc.next();
		System.out.print("enter book price :");
		int price=sc.nextInt();
		
		Book b1=new Book();
		Book b2=new Book(bookid);
		Book b3=new Book(bookid,title,price);
		System.out.println("before updatation");
		System.out.println(b3.getbookid());
		System.out.println(b3.gettitle());
		System.out.println(b3.getprice());
	
		System.out.println(" you want modify the value (1:0)");
	   int yes=sc.nextInt();
	   if(yes==1)
	   { 	
		    b3.setprice(50);
			b3.settitle("kgf");
	   
				
		
		System.out.println("after  the  updatation is done");
		System.out.println(b3.getbookid());
		System.out.println(b3.gettitle());
	
		System.out.println(b3.getprice());

	   }
	   else
		   System.out.println("end");
	}
}
