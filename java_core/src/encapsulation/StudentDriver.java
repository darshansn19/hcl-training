package encapsulation;
public class StudentDriver {

	public static void main(String[] args)
	{
		Student s1=new Student(10,"ram",20);
		System.out.println("before updating: ");
		System.out.println(s1.getid());
		System.out.println(s1.getname());
		System.out.println(s1.getage());
		
		
		System.out.println("after updatation");
		s1.setid(30);
		s1.setname("darshan");
		s1.setage(22);
		
		System.out.println(s1.getid());
		System.out.println(s1.getname());
		System.out.println(s1.getage());
		
		
	}

}