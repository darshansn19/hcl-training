package encapsulation;

public class Book 
{
	private int bookid;
	private String title;
	private int price;
	
	Book()
	{
	}
	Book(int bookid)
	{
		this.bookid=bookid;
	}
	Book(int bookid,String title,int price)
	{
		this(bookid);
		this.title=title;
		this.price=price;
	}
	
	public  int getbookid()
	{
		return bookid;
	}
	
	public String gettitle()
	{
		return title;
	}
	public void settitle(String title)
	{
		this.title=title;
	}
	
	public int getprice()
	{
		return price;
	}
	public void setprice(int price)
	{
		this.price=price;
	}
	
}