package inheritance;

public class Book extends Author
{
	String name;
	double price;
	Book(String authorName,int age,String place,String name,double price)
	{
		super(authorName,age,place);
		this.name=name;
		this.price=price;
	}
}