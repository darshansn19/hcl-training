package inheritance;

public class FruitDriver {

	public static void main(String[] args) {
		Fruit f=new Fruit("red",100);
		System.out.println(f.colour);
		System.out.println(f.price);
		
		Apple a=new Apple("red",200,"green",150);
		System.out.println(a.colour);
		System.out.println(a.price);
		System.out.println(a.colour1);
		System.out.println(a.price1);
		
	}

}
