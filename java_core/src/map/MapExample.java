package map;

import java.util.HashMap;
public class MapExample
{
	public static void main(String[] args) {
		HashMap<Integer,String> h1=new HashMap();//it can be used as both generic or non generic
		h1.put(1, "dar");
		h1.put(2, "sar");
		h1.put(3, "sede");
		h1.put(4, "anduravi");//adding an object to the map
		System.out.println(h1);
		h1.remove(3);// removing a map
		System.out.println(h1);
		
		System.out.println(h1.containsKey(2));//check if object based on key is present or not
		
		System.out.println(h1.containsValue("ram"));//check if object based on value is present or not
		System.out.println(h1.values());//only printing value
		System.out.println(h1.keySet());//only printing keys
		System.out.println(h1.entrySet());//to display all entries.
		
	}

}
