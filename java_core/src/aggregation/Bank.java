package aggregation;
import java.util.Scanner;
public class Bank 
{
	String name;
	String branch;
	Account a;
	
	Bank(String name,String branch,Account a)
	{
		this.name=name;
		this.branch=branch;
		this.a=a;
	}
	public void creatAccount()
	{
		if(a==null)
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("enter the account number: ");
			long acno=sc.nextLong();
			System.out.println("enter the account name: ");
			String acName=sc.next();
			System.out.println("enter the balance: ");
			double Balance=sc.nextDouble();
			
			this.a=new Account(acno,acName,Balance);
			System.out.println("account created successfully...!");
		}
		else
		{
			System.out.println("already account exits");
		}
	}
	public void deleteAccount()
	{
		if(a!=null)
		{
			this.a=null;
			System.out.println("account is succesfully deleted..... ");
		}
		else
			System.out.println("account doesn't exist");
	}
	
	public void withdrawal() //throws InsufficientBalance
	{
		
	if(a!=null) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the amount to be withdrawed: ");
		double amount=sc.nextDouble();

			if(amount<a.getbalance())
			{
				
			
			a.setBalanceWithdraw(amount);
			}
		else
			try {
			throw new InsufficientBalance();
			}
			catch(InsufficientBalance o)
			{
				System.out.println("balance is too low");
			}
	}
	else
			System.out.println("account doesn't exist");
			
	}
	public void deposit()
	{
		if(a!=null)
		{
			Scanner sc=new Scanner(System.in);
			System.out.println("enter the amount to be deposited");
			double newvalue=sc.nextDouble();
			a.setBalanceDeposit(newvalue);
		}
		else
			System.out.println("account doesn't exist");
	}
	
	public void enquiry()
	{
		if(a!=null)
		{
			System.out.println("balance is "+ a.getbalance());
		}
		else
			System.out.println("account doesn't exist");
	}
	public void accountDetails()
	{
		if(a!=null)
		{
			a.getAccountdetails();
		}
		else
			System.out.println("account does not exist ");
	
	}
	public void bankDetails()
	{
		System.out.println("---------Bank Details----------");
		System.out.println("bankname is :"+name);
		System.out.println("bank branch: "+branch);
	}
}
