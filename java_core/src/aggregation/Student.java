package aggregation;

public class Student 
{
	 private int id;
	 private String name;
	 private long phono;
	 Student()
	 {
		 
	 }
	 Student(int id)
	 {
		 this.id=id;
	 }
	 Student(int id,String name)
	 {
		 this(id);
		 this.name=name;
	 }
	 Student(int id,String name,long phono)
	 {
		 this(id,name);
		 this.phono=phono;
	 }
	 
	 public int getid()
	 {
		 return id;
	 }
	 public String getname()
	 {
		 return name;
	 }
	 
	 public long getphono()
	 {
		 return phono;
	 }
	 public void setphono(long phono)
	 {
		 this.phono=phono;
	 }
	 public void setname(String name)
	 {
		 this.name=name;
	 }
	public void getStudentDetails()
	{
		System.out.println("student id: "+getid());
		System.out.println("student name: "+getname());
		System.out.println("student phone "+getphono());
	}
}