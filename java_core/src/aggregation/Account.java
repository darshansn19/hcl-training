package aggregation;

public class Account
{
	private  long acno;
	private String acName;
	private double Balance;
	Account(long acno,String acName,double Balance)
	{
		this.acName=acName;
		this.acno=acno;
		this.Balance=Balance;
	}
	public long getacno()
	{
		return acno;
	}
	public String getacName()
	{
		return acName;
		
	}
	public double getbalance()
	{
		return Balance;
	}
	
	public void setBalanceDeposit(double amount)
	{
		this.Balance=this.Balance+amount;
	}

	public void setBalanceWithdraw(double newvalue)
	{
		this.Balance=this.Balance-newvalue;
	}
	
	public void getAccountdetails() 
	{
		System.out.println("account number: "+getacno());
		System.out.println("account name: "+getacName());
		System.out.println("account balance: "+getbalance());
	}
}
