package aggregation;
import java.util.Scanner;
public class School
{
	String schoolname;
	String location;
	Student s;
	School(String schoolname,String location,Student s)
	{
		this.location=location;
		this.schoolname=schoolname;
		this.s=s;
	}
	public void addstudent()
	{
		if(s==null)
		{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the id: ");
		int id=sc.nextInt();
		System.out.println("enter student name: ");
		String name=sc.next();
		System.out.println("enter the phone number: ");
		long phono=sc.nextLong();
		//System.out.println("student added successfully");
		
		this.s=new Student(id,name,phono);
		System.out.println("Student admission succefully");
		}
		else
			System.out.println("there is no space for 2nd object");
	}
	public void removeStudent()
	{
		if(s!=null)
		{
		this.s=null;
		System.out.println("Student removed from school");
		}
		else
			System.out.println("\nthere is no student to remove");
			
	}
	public void updateStudentName()
	{
		if(s!=null)
		{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the new name to be updated: ");
		String newname=sc.next();
		System.out.println(".................");
		s.setname(newname);
		System.out.println("student name updated successfully");
		}
		else
			System.out.println("student doesn't exist");
	}
	public void displayStudent()
	{
		if(s!=null)
			s.getStudentDetails();
		else
			System.out.println("student doesn't exist");
	}
	
	public void displaySchoolDetails()
	{
		System.out.println("school details");
		System.out.println("school name : "+schoolname);
		System.out.println("school location: "+location);
		
	}
}

