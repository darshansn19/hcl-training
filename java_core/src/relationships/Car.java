package relationships;

public class Car
{
	String model;
	double price;
	String colour;
	Engine e;
	
	Car(String model,double price,String colour,Engine e)
	{
		this.model=model;
		this.price=price;
		this.colour=colour;
		this.e=e;
	}
	
	public void getCarDetails()
	{
		System.out.println("model: "+model);
		System.out.println("price: "+price);
		System.out.println("colour: "+colour);
	}
	
}
