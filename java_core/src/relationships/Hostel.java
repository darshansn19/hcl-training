package relationships;

public class Hostel 
{
	private String hname;
	private	String loc;
	 Student s;
	Hostel()
	{
		
	}
	Hostel(String hname)
	{
		this.hname=hname;
	}
	Hostel(String hname,String loc)
	{
		this(hname);
		this.loc=loc;
	}
	Hostel(String hname,String loc,Student s)
	{
		this(hname,loc);
		this.s=s;
	}
		public String gethname()
		{
			return hname;
		}
		public String getloc()
		{
			return loc;
		}
		public void displayHostelDisplay()
		{
			System.out.println("hostel name: "+gethname());
			System.out.println("hostel location: "+ getloc());
		}

}
