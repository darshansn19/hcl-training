package relationships;
import java.util.Scanner;
public class HostelDriver {

	public static void main(String[] args)
	{
		
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the hostel name: ");
		String hname=sc.next();
		System.out.println("enter the hostel location: ");
		String loc=sc.next();
		System.out.println("enter the student name: ");
		String name=sc.next();
		System.out.println("enter the student id: ");
		int id=sc.nextInt();
		System.out.println("enter the student age: ");
		int age=sc.nextInt();
		System.out.println("enter the phone number: ");
		long phno=sc.nextLong();
		Hostel h=new Hostel(hname,loc,new Student(name,id,age,phno));
		h.displayHostelDisplay();
		h.s.getStudentDetails();
		

	}

}
