package relationships;

public class Mobile
{
	String model;
	double price;
	Sim s;
	
	Mobile(String model,double price,Sim s)
	{
		this.model=model;
		this.price=price;
		this.s=s;
	}
	public  void getMobileDetails()
	{
		System.out.println("modelname is : "+model);
		System.out.println("mobile price is "+price);
	}
}
