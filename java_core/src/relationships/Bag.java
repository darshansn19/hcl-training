package relationships;

public class Bag 
{
	String colour;
	int price;
	Book1 b;
	public Bag(String colour, int price, Book1 b)
	{
		this.colour=colour;
		this.price=price;
		this.b=b;
	}

	public void displayBagDetails()
	{
		System.out.println(" bag colour is : "+colour);
		System.out.println("bag price is : "+price);
	}
}
