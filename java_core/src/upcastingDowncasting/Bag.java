package upcastingDowncasting;
import java.util.Scanner;
public class Bag 
{
	Ball b;
	public void addBall()
	{	
		if(b==null)
		{
			Scanner sc=new Scanner(System.in);
			
		System.out.println("which game you want to play: ");
		System.out.println("we have....\n1.basket ball\n2.tennis ball");
		System.out.println("enter your choice: ");
		int choice=sc.nextInt();
		if(choice==1)
		{
			b=new BasketBall(10);
			System.out.println("basket ball object is created");
		}
		else if(choice==2)
		{
			b=new Tennis(10);
			System.out.println("Tennis ball object is created");
		}
		else
		{
			System.out.println("select either 1 or 2 ");
		}
		}
		else
			System.out.println("object is already created");
		
	}
	public void removal()
	{
		
		if(b!=null) 
		{
		if(b instanceof BasketBall)
		{
			b=null;
			System.out.println("basket ball is removed");
		}
		else
		{
			b=null;
			System.out.println("tennis ball is removed");
		}
		}
		else
			System.out.println("no balls in bag");
	}
	
	public  void checkbag()
	{
		if(b==null)
		{
			System.out.println("bag is empty");
		}
		else
		{
			System.out.println("bag consists of ball");
		}
	}
	public void showGame()
	{
		if(b!=null) {
		if(b instanceof BasketBall)
		{
			System.out.println("Basket ball is being played");
			BasketBall bb=(BasketBall) b;
			bb.displayBasketball();
		}
		else
		{

			System.out.println("Tennis is being played");
			Tennis tt=(Tennis) b;
			tt.displayTennisball();
		}
		}
		else
			System.out.println("there is no game to play");
	}
}
