package upcastingDowncasting;

public class Prime extends Cab
{
	double price;
	Prime(String from,String to,double price)
	{
		super(from,to);
		this.price=price;
	}
	public void getPrimeDetails()
	{
		System.out.println("prime price is: "+price);
	}
	
}
