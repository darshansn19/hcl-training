package upcastingDowncasting;
import java.util.Scanner;
public class Ola 
{
	Cab c;
	Scanner sc=new Scanner(System.in);
	public void bookcab()
	{
		if(c==null) {
		System.out.println("enter the from place");
		String from=sc.next();
		System.out.println("enter the to place");
		String to=sc.next();
		System.out.println("select the vehicle: ");
		System.out.println("we have....\n1.mini\n2.prime\n3.sedan");
		System.out.println("enter your choice: ");
		int choice=sc.nextInt();
		if(choice==1)
		{
			
					
			c=new Mini(from,to,50);
			System.out.println("mini  object is created");
		}
		else if(choice==2)
		{
			
			c=new Prime(from,to,100);
			System.out.println("prime object is created");
		}
		else if(choice==3)
		{
			c=new Sedan(from,to,200);
			System.out.println("prime object is created");
		}
		else
			System.out.println("select anyone vehicle");
		}
		else
			System.out.println("cab service is not available");
		
	}
	public void cancel()
	{
		if(c!=null)
		{
			if(c instanceof Mini) {
			c=null;
			System.out.println("mini get canceld sucessfully");
			}
			else if(c instanceof Prime) 
			{
				c=null;
				System.out.println("prime get canceld sucessfully");	
			}
			else
			{
			c=null;
			System.out.println(" sedan get canceld sucessfully");
			}
		}
		else
			System.out.println("book a cab first");
		
	}
	 public void cabDetails()
	 {
		 if(c!=null)
		 {
			 System.out.println("from place is :"+c.from);
			 System.out.println("to place is : "+c.to);
			
			 if(c instanceof Mini)
			 {
				 Mini m=(Mini) c;
				 m.displayMiniDetails();
				 
			 }
			 else if(c instanceof Prime)
			 {
				 Prime p=(Prime)c;
				 p.getPrimeDetails();

				 
			 }
			 else
			 {
				 Sedan s=(Sedan)c;
				 s.getSedan();
			  
			 }
		 }
		 else
			 System.out.println("cab is empty");
	 }
	 
	

}
