package upcastingDowncasting;

public class Mini extends Cab
{
	double price;
	
	Mini(String from,String to, double price)
	{
		super(from,to);
		this.price=price;
	}
	public void displayMiniDetails()
	{
		System.out.println("price of mini is: "+price);
	}
	

}
