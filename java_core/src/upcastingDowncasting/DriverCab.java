package upcastingDowncasting;
import java.util.Scanner;
public class DriverCab {

	public static void main(String[] args) 
	{
		Ola c=new Ola();
		Scanner sc=new Scanner(System.in);
		boolean control=true;
		while(control)
		{
			System.out.println("1.book cab\n2.cancel the book\n3.cab details\n4.exit");
			System.out.println("enter the choice");
			int ch=sc.nextInt();
			switch(ch)
			{
			case 1:
			{
				c.bookcab();
				break;
			}
			case 2:
			{
				c.cancel();
				break;
			}
			case 3:
			{
				c.cabDetails();
				break;
			}
			case 4:
			{
				control=false;
				System.out.println("service closed");
			}
			}
			
		}
	}

}
