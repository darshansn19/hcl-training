package collection;

public class Student implements Comparable<Object>
{
	int id;
	String name;
	int age;
	long phono;
	public Student(int id, String name, int age, long phono) 
	{
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.phono = phono;
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", phono=" + phono + "]";
	}
	
	
	public int  compareTo(Object o)
	{
		Student s=(Student)o;
		if(this.id==s.id)
		{
			return 0;
		}
		else if(this.id>s.id)
		{
			return 1;
		}
		else
			return -1;
	}
	
}
