package collection;

public class TIcket
{
	String Arrival;
	String Departure;
	String name;
	int ticno;
	
	public TIcket(String arrival, String departure, String name, int ticno) {
		super();
		Arrival = arrival;
		Departure = departure;
		this.name = name;
		this.ticno = ticno;
	}
	@Override
	public  String toString() {
		return "TIcket [Arrival=" + Arrival + ", Departure=" + Departure + ", name=" + name + ", ticno=" + ticno + "]";
	}
	
	

}
