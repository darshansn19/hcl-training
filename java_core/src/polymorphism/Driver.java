package polymorphism;

public class Driver {

	public static void main(String[] args)
	{
		Parent p=new Parent();
		p.display();
		//System.out.println(p.a);
		//System.out.println(p.b);

		
		Child c1=new Child();
		c1.display();
		//System.out.println(c1.a);
		//System.out.println(c1.b);
		
		Parent c=new Child();
		c.display();
		//System.out.println(c.a);
		//System.out.println(c.b);
		
		
	}
}
