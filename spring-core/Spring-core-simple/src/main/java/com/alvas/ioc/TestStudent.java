package com.alvas.ioc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class TestStudent {
	public static void main(String[] args) {
		
	
	ClassPathResource classPathResource=new ClassPathResource("myspring.xml");
	BeanFactory beanFactory=new XmlBeanFactory(classPathResource);
	Student student=(Student)beanFactory.getBean("student");
	student.study();
	System.out.println(student.id);
	System.out.println(student.name);
	System.out.println(student.email);
	}

}
