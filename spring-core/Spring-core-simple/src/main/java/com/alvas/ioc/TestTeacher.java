package com.alvas.ioc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class TestTeacher {
	public static void main(String[] args) {
		
	
	ClassPathResource classPathResource=new ClassPathResource("myspring.xml");
	BeanFactory beanFactory= new XmlBeanFactory(classPathResource);
	Teacher teacher=(Teacher) beanFactory.getBean("teacher");
	 teacher.teach();
	 System.out.println(teacher.id);
	 System.out.println(teacher.name);
	 System.out.println(teacher.email);

}
}