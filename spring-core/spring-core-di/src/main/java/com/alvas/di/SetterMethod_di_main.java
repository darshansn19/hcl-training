package com.alvas.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterMethod_di_main {
	public static void main(String[] args) {
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("myspring.xml");
		Person person=(Person)applicationContext.getBean("person");
		System.out.println(person);
		
	}
}
