package com.alvas.ac;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestTable {
	public static void main(String[] args) {

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("myspring.xml");
		Table table = (Table) applicationContext.getBean("tab");
		System.out.println(table.id);
		System.out.println(table.color);
		System.out.println(table.price);

	}
}
