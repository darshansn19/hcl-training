package com.alvas.ac;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestHotel {
	public static void main(String[] args) {
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("myspring.xml");
		Hotel hotel=(Hotel)applicationContext.getBean("hotel");
		System.out.println(hotel.id);
		System.out.println(hotel.name);
		System.out.println(hotel.dish);
		System.out.println(hotel.price);
	}
}
