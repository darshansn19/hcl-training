package com.alvas.ac;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Car {
	public Engine engine;
	
	/*@Autowired
	Car( Engine engine){
		this.engine=engine;
	}*/

	@Autowired
	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	void move()
	{ engine.start();
		System.out.println("car is moving1");
	}
}
