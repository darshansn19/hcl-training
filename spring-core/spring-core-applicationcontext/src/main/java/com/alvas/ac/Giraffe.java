package com.alvas.ac;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Giraffe {
	@Value(value="11")
	int id;
	@Value(value="white")
	String color;
	@Value(value="18")
	double height;
}
