package com.alvas.ac;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Hotel {
	int id;
	String name;
	String dish;
	String price;
	
	@Value(value="5")
	public void setId(int id) {
		this.id = id;
	}
	@Value(value="sarathi")
	public void setName(String name) {
		this.name = name;
	}
	@Value(value="dosa")
	public void setDish(String dish) {
		this.dish = dish;
	}
	@Value(value="5000")
	public void setPrice(String price) {
		this.price = price;
	}
	
}
