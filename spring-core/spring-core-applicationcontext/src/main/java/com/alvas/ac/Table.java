package com.alvas.ac;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="tab")
public class Table {

	int id;
	String color;
	double price;
	public Table(@Value(value="12")int id, @Value(value="green")String color, @Value(value="1200")double price) {
		
		this.id = id;
		this.color = color;
		this.price = price;
	}
	
}
