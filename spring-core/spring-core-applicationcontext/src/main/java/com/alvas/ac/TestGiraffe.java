package com.alvas.ac;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestGiraffe {
	public static void main(String[] args) {
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("myspring.xml");
		Giraffe giraffe=(Giraffe)  applicationContext.getBean("giraffe");
		 System.out.println(giraffe.id);
		 System.out.println(giraffe.color);
		 System.out.println(giraffe.height);
	}
}
