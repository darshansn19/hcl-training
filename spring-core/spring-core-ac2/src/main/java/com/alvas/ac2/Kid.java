package com.alvas.ac2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Kid {
	@Autowired
	@Qualifier("butterScotch")
	IceCream iceCream;
	void cry()
	{
		System.out.println("kid is crying");
		iceCream.eat();
		System.out.println("kid is feeling happy");
	}

}
