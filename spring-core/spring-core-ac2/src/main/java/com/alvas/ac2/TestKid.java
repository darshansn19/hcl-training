package com.alvas.ac2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestKid {
	public static void main(String[] args) {

		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
		Kid kid = (Kid) applicationContext.getBean("kid");
		kid.cry();
	}
}
