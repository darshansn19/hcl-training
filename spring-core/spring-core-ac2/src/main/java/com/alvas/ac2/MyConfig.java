package com.alvas.ac2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.alvas.ac2")
public class MyConfig {
	
	@Bean("car")
	Car getCar()
	{
		return new Car();
	}
}
