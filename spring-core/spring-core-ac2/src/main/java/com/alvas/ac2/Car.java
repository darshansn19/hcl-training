package com.alvas.ac2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class Car {
	
		@Autowired
		Engine engine;

		
		void move()
		{ engine.start();
			System.out.println("car is moving");
		}
}
