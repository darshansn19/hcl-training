package com.alvas.ac2;

import org.springframework.stereotype.Component;

@Component

public interface IceCream {
	void eat();
}
