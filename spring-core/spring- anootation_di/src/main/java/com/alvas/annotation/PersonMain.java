package com.alvas.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonMain {
	public static void main(String[] args) {
		// ApplicationContext applicationContext=new
		// ClassPathXmlApplicationContext("myspring.xml");
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfiguration.class);
		Person person = (Person) applicationContext.getBean("person");
		System.out.println(person);
	}

}
