package com.alvas.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.alvas.annotation")
public class MyConfiguration {

}
