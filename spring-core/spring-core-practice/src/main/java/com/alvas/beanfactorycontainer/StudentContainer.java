package com.alvas.beanfactorycontainer;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class StudentContainer {
	
	public static void main(String[] args) {
		ClassPathResource classPathResource=new  ClassPathResource("myspring.xml");
		
		BeanFactory beanFactory=new XmlBeanFactory(classPathResource);
		 Student student=(Student)beanFactory.getBean("student");
		 
		student.display();
		
	}

}
