package com.alvas.ApplicationConext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alvas.beanfactorycontainer.Student;

public class StudentApplicationContext {
	public static void main(String[] args) {
		
	
	
	ApplicationContext applicationContext=new ClassPathXmlApplicationContext("myspring.xml");
	Student student=(Student)applicationContext.getBean("student");
	student.display();
	}
}
