package com.alvas.dao.person;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.alvas.dto.BankAccount;
import com.alvas.dto.Person;
public class TestBankdao {

	
		static EntityManagerFactory entityManagerFactory =Persistence.createEntityManagerFactory("uday");

		public Person saveBank(BankAccount bankAccount, int pid) {
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EntityTransaction entityTransaction = entityManager.getTransaction();
			Person person = entityManager.find(Person.class, pid);
			if (person != null) {
				bankAccount.setPerson(person);
				entityTransaction.begin();
				entityManager.persist(bankAccount);
				entityTransaction.commit();
				return entityManager.find(Person.class, pid);
			}
			else
				return null;

		}
		
		public BankAccount getBankById(int id)
		{
			EntityManager entityManager=entityManagerFactory.createEntityManager();
			BankAccount account=entityManager.find(BankAccount.class,id);
			return account;
			
		}

	}
	

