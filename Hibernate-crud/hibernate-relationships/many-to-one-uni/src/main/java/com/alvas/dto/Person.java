package com.alvas.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Person {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
 private int pid;
 private String name;
 private String email;
 private long phone;
 private int age;
public int getId() {
	return pid;
}
public void setId(int pid) {
	this.pid = pid;
}
@Override
public String toString() {
	return "Person [pid=" + pid + ", name=" + name + ", email=" + email + ", phone=" + phone + ", age=" + age + "]";
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public long getPhone() {
	return phone;
}
public void setPhone(long phone) {
	this.phone = phone;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
 

}
