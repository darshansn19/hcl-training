package com.alvas.dao.person;

import java.security.PublicKey;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.alvas.dto.Person;

public class TestPersondao {
	static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");

	public Person savePerson(Person p) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();

		entityTransaction.begin();
		entityManager.persist(p);
		entityTransaction.commit();
		return entityManager.find(Person.class, p.getId());

	}

	public Person getPersonById(int id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Person p = entityManager.find(Person.class, id);
		return p;

	}

	public List<Person> getPersonByName(String name) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		String s = "select s from Perosn p where s.name=?1";
		Query query = entityManager.createQuery(s);
		query.setParameter(1, name);

		List<Person> person = query.getResultList();
		if (person != null) {
			return person;
		} else

			return null;

	}
	
	public boolean deletePersonById(int id)
	{
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction=entityManager.getTransaction();
		
		Person p=entityManager.find(Person.class, id);
		if(p!=null)
		{entityTransaction.begin();
			entityManager.remove(p);
			entityTransaction.commit();
			return true;
		}
		else{
			
		return false;
		}
	}
	
	public Person updatePersonById(int id,Person p)
	{
		 EntityManager entityManager=entityManagerFactory.createEntityManager();
		EntityTransaction   entityTransaction=entityManager.getTransaction();
		
		Person person=entityManager.find(Person.class, id);
		if(person!=null){
			p.setId(person.getId());
			entityTransaction.begin();
			entityManager.merge(p);
			entityTransaction.commit();
			return  entityManager.find(Person.class, id);
			
		}else
		return null;
	}
	
	

}
