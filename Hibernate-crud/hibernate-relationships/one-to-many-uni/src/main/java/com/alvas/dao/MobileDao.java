package com.alvas.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.alvas.dto.Mobile;
import com.alvas.dto.Sim;

import antlr.collections.List;

public class MobileDao {
	static EntityManagerFactory entityManagerFactor = Persistence.createEntityManagerFactory("uday");

	public Mobile saveMobileSim(Mobile mobile) {
		EntityManager entityManager = entityManagerFactor.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		java.util.List<Sim> Sims = mobile.getSims();
		entityTransaction.begin();
		for (Sim s : Sims) {
			entityManager.persist(s);
		}
		entityManager.persist(mobile);
		entityTransaction.commit();
		return entityManager.find(Mobile.class, mobile.getMid());

	}

	public Mobile getMobileId(int id) {

		EntityManager entityManager = entityManagerFactor.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();

		return entityManager.find(Mobile.class, id);

	}

	public boolean deleteById(int id) {
		EntityManager entityManager = entityManagerFactor.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Mobile mobile = entityManager.find(Mobile.class, id);

		if (mobile != null) {
			java.util.List<Sim> list = mobile.getSims();
			entityTransaction.begin();
			// entityManager.remove(mobile);
			for (Sim s : list) {
				entityManager.remove(s);
			}
			entityManager.remove(mobile);
			entityTransaction.commit();
			System.out.println("deleted");
			return true;
		} else {
			return false;
		}

	}

	public Mobile updateMobile(int id, Mobile mobile) {
		EntityManager entityManager = entityManagerFactor.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Mobile mobile2 = entityManager.find(Mobile.class, id);
		if (mobile2 != null) {
			mobile.setMid(mobile2.getMid());
			entityTransaction.begin();
			entityManager.merge(mobile);
			entityTransaction.commit();
			return entityManager.find(Mobile.class, mobile2.getMid());

		} else
			return null;

	}

}
