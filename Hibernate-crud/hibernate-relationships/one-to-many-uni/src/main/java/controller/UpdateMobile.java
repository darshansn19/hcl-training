package controller;

import java.util.ArrayList;

import com.alvas.dao.MobileDao;
import com.alvas.dto.Mobile;
import com.alvas.dto.Sim;

import antlr.collections.List;

public class UpdateMobile {
	public static void main(String[] args) {
		 Mobile m = new Mobile();
		 m.setName("vivoy3");
		 m.setBrand("vivo");
		 m.setPrice(10000);
		
		java.util.List<Sim> sims=new ArrayList<>();
		 Sim sim = new Sim();
		 sim.setProvider("idea");
		 sim.setType("4G");
		 sims.add(sim);
		
		 Sim sim2 = new Sim();
		 sim2.setProvider("docomo");
		 sim2.setType("5G");
		 sims.add(sim2);
		 m.setSims(sims);
		
		 MobileDao dao=new MobileDao();
		 Mobile mobile=dao.updateMobile(1, m);
		 if(mobile!=null)
		 {
		 System.out.println("updated");
		 }
		 else
		 System.out.println("not updated");
	}

}
