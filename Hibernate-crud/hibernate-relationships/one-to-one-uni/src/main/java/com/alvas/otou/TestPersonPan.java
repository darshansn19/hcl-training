package com.alvas.otou;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TestPersonPan {
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();

		
		Pan pan = new Pan();
		pan.setAddresss("dvg");
		pan.setPanNumber("abcd1234");
		
		Person person = new Person();
		person.setAge(25);
		person.setEmail("darshan@gmail.com");
		person.setName("darshan");
		person.setPhone(78956231654l);
		person.setPan(pan);
		

		entityTransaction.begin();
		entityManager.persist(person);
		entityManager.persist(pan);
		entityTransaction.commit();
	}

}
