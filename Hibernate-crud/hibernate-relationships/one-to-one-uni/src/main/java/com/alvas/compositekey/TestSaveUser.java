
package com.alvas.compositekey;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TestSaveUser {
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		UserId userId=new UserId();
		userId.setEmail("simha@gmail.com");
		userId.setPhone("8656556666");
		 
		User use=new User();
		use.setAge(20);
		use.setName("simha");
		use.setPassword("nanee");
		use.setUserId(userId);
		
		entityTransaction.begin();
		entityManager.persist(use);
		entityTransaction.commit();
		
		
		

	}
}
