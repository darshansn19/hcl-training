package com.alvas.otou;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DeletePerson {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction=entityManager.getTransaction();
		
//		Person person=entityManager.find(Person.class, 1);
//		Pan pan=entityManager.find(Pan.class, 1);
		
		Person person=entityManager.find(Person.class, 3);
		if(person!=null)
		{
			entityTransaction.begin();
			//entityManager.remove(pan);
			entityManager.remove(person);
			entityTransaction.commit();
		
		}
		else
			System.out.println();
		

	}

}
