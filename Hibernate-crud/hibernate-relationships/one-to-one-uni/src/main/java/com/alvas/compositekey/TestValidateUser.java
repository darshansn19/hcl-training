package com.alvas.compositekey;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class TestValidateUser {
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		String s = "select u from User u where u.userId.email=?1 or u.userId.phone=?2 and u.password=?3";
		Query query = entityManager.createQuery(s);
		
		query.setParameter(1, "simha@gmail.com");
		 query.setParameter(2, "865556666");
		query.setParameter(3, "nane");

		List<User> users = query.getResultList();
		if (users.size() > 0) {
			User user = users.get(0);
			System.out.println(user);

		} else
			System.out.println("email or password wrong");
	}

}
