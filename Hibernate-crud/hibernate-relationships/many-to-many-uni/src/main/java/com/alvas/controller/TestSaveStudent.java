package com.alvas.controller;

import com.alvas.dao.StudentDao;
import com.alvas.dto.Student;

public class TestSaveStudent {
	public static void main(String[] args) {
		Student student=new Student();
		student.setAge(21);
		student.setEmail("my@gmail.com");
		student.setName("darshan");
		StudentDao dao=new StudentDao();
		
		Student student2=dao.SaveStudent(student);
		if(student2!=null)
			System.out.println("inserted");
		else
			System.out.println("not inserted");
		}

}
