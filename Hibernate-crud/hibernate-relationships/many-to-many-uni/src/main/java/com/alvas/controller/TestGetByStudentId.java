package com.alvas.controller;

import com.alvas.dao.StudentDao;
import com.alvas.dto.Student;

public class TestGetByStudentId {
	public static void main(String[] args) {
		StudentDao dao=new StudentDao();
		Student student=dao.getStudentById(1);
		if(student!=null)
			System.out.println(student);
		else
			System.out.println("not found");
	}

}
