package com.alvas.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.hql.internal.ast.exec.DeleteExecutor;

import com.alvas.dto.Courses;

public class CourseDao {
	static EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("uday");
	static EntityManager entityManager=entityManagerFactory.createEntityManager();
	
	public Courses saveCouurse(Courses c)
	{
		EntityTransaction entityTransaction=entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.persist(c);
		entityTransaction.commit();
		return entityManager.find(Courses.class, c.getCid());
	}
	
	public  Courses getCoursesByIdD(int id)
	{
		Courses courses=entityManager.find(Courses.class, id);
		if(courses!=null)
			return courses;
		else
		
		 return null;
	}
	
	public Courses updateCourse(int id,Courses courses){
		EntityTransaction entityTransaction=entityManager.getTransaction();
		Courses courses2=entityManager.find(Courses.class, id);
		if(courses2!=null)
		{
			courses.setCid(courses2.getCid());
			entityTransaction.begin();
			entityManager.merge(courses);
			entityTransaction.commit();
			return entityManager.find(Courses.class, courses.getCid());
		}else
			return null;
				
	}
	public  boolean deleteCourses(int id)
	{
		EntityTransaction entityTransaction=entityManager.getTransaction();
		Courses courses=entityManager.find(Courses.class, id);
		if(courses!=null)
		{
			entityTransaction.begin();
			entityManager.remove(courses);
			entityTransaction.commit();
			return true;
		}
		else
			return false;
	}
	

}
