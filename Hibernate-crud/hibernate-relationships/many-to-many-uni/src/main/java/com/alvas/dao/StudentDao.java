package com.alvas.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.alvas.dto.Courses;
import com.alvas.dto.Student;

public class StudentDao {
	static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");
	static EntityManager entityManager = entityManagerFactory.createEntityManager();

	public Student SaveStudent(Student student)
	{	EntityTransaction entityTransaction=entityManager.getTransaction();
		List<Courses> l1=new ArrayList<Courses>();
		Courses course1=entityManager.find(Courses.class, 1);
		Courses course2=entityManager.find(Courses.class,2);
		Courses course3=entityManager.find(Courses.class, 3);
		if(course1!=null && course2!=null && course3!=null)
		{
			l1.add(course1);
			l1.add(course2);
			l1.add(course3);
		}
		student.setCourses(l1);
		entityTransaction.begin();
		 entityManager.persist(student);
		 entityTransaction.commit();
		 return entityManager.find(Student.class,student.getSid());
		
	}
	public Student getStudentById(int id)
	{
		
		Student student=entityManager.find(Student.class, id);
		if(student!=null)
		{
			return student;
		}
		else
			return null;
		
	}
	public  boolean deleteStudent(int id)
	{
		EntityTransaction entityTransaction=entityManager.getTransaction();
		Student student =entityManager.find(Student.class, id);
		if(student!=null)
		{
			entityTransaction.begin();
			entityManager.remove(student);
			entityTransaction.commit();
			return true;
		}
		else
			return false;
	}
	
	public Student updateStudent(int id,Student student ){
		EntityTransaction entityTransaction=entityManager.getTransaction();
		Student student1=entityManager.find(Student.class, id);
		if(student!=null)
		{
			student.setSid(student1.getSid());
			entityTransaction.begin();
			entityManager.merge(student);
			entityTransaction.commit();
			return entityManager.find(Student.class, student.getSid());
		}else
			return null;
		
				
	}
}

