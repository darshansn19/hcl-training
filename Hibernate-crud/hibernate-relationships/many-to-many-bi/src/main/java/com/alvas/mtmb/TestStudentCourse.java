
package com.alvas.mtmb;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TestStudentCourse {
	public static void main(String[] args) {
		Student student1 = new Student();
		student1.setName("darsh");
		student1.setAge(21);
		student1.setEmail("aa@gmail.com");

		Student student2 = new Student();
		student2.setName("darsh");
		student2.setAge(21);
		student2.setEmail("aa@gmail.com");

		

		Courses courses1 = new Courses();
		courses1.setCname("c++");
		courses1.setFee(5000);
		
		Courses courses2 = new Courses();
		courses2.setCname("java");
		courses2.setFee(5000);

		List<Student> list = new ArrayList<Student>();
		list.add(student1);
		list.add(student2);
		
		List<Courses> courses = new ArrayList<Courses>();

		courses.add(courses1);
		courses.add(courses2);
		
		student1.setCourses(courses);
		student2.setCourses(courses);
		
		courses1.setStudents(list);
		courses2.setStudents(list);

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.persist(student1);
		entityManager.persist(student2);
		entityTransaction.commit();
	}

}
