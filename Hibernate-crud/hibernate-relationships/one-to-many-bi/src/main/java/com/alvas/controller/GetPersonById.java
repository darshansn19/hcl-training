package com.alvas.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.alvas.dao.TestPersondao;
import com.alvas.dto.Person;

public class GetPersonById {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("uday");
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		
		Person person=entityManager.find(Person.class, 1);
		if(person !=null)
			System.out.println(person);
		else
			
			System.out.println("not found");
	}
}
