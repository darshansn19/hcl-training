package com.alvas.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class RemoveById {
	public static void main(String[] args) {
		Student student = null;
		Configuration configuration = new Configuration().configure().addAnnotatedClass(Student.class);
	
		SessionFactory factory = configuration.buildSessionFactory();
		Session session = factory.openSession();
		student =session.get(Student.class, 8
				);
		if(student!=null)
		{
		Transaction transaction = session.beginTransaction();
		session.remove(student);
				transaction.commit();
		}
		else
			System.out.println("not found");
	}
}
