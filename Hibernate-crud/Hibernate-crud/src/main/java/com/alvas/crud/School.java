package com.alvas.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class School {
	public static void main(String[] args) {
		Student student = new Student();
		
		student.setAge(4);
		student.setName("sarathi");

		
		Configuration configuration = new Configuration().configure().addAnnotatedClass(Student.class);
		

		SessionFactory factory = configuration.buildSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();
		session.save(student);
		transaction.commit();
	}
}
