package com.alvas.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class GetById {
	public static void main(String[] args) {
		Student student = null;

		Configuration configuration = new Configuration().configure().addAnnotatedClass(Student.class);

		SessionFactory factory = configuration.buildSessionFactory();
		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();
		student = session.get(Student.class, 0);
		transaction.commit();
		if (student != null)
			System.out.println(student);
		else
			System.out.println("not found");

	}
}
