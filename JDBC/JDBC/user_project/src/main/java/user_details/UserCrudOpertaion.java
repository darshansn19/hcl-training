package user_details;

//data access object
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import com.alvas.User.dto.User;
import com.mysql.cj.protocol.Resultset;

public class UserCrudOpertaion {
	Scanner sc = new Scanner(System.in);

	public Connection getConnection() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user", "root", "root");
		return connection;
	}

	public void insertUser(int id, String name, long phone) throws Exception {

		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("insert into user_table values(?,?,?)");
		st.setInt(1, id);
		st.setString(2, name);
		st.setLong(3, phone);
		st.execute();
		st.close();
		con.close();
	}

	public void UpdateStudent(int id, User user) throws Exception {

		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("update user_table set name=?,phone=? where id=?");
		st.setString(1, user.getName());
		st.setLong(2, user.getPhone());
		st.setInt(3, id);
		st.execute();
		st.close();
		con.close();

	}

	public User getuserById(int id) throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("select * from user_table where id=?");
		st.setInt(1, id);
		ResultSet res = st.executeQuery();

		User user = new User();

		while (res.next()) {
			user.setId(res.getInt(1));
			user.setName(res.getString(2));
			user.setPhone(res.getLong(3));
		}
		return user;
	}

	public void getAlluser() throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("select *from user_table");
		ResultSet res = st.executeQuery();

		while (res.next()) {
			System.out.println(res.getInt(1) + " " + res.getString(2) + " " + res.getLong(3));
		}

	}

	public void deletUser(int id) throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("delete from user_table where id=?");
		st.setInt(1, id);
		st.executeUpdate();
		st.close();
		con.close();

	}

}
