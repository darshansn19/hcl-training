package com.alvas.person.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.alvas.person.dto.Person;

public class PersonDao {

	public Connection getConnection() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/person_details", "root", "root");
		return connection;
	}

	public void insertPerson(Person person) throws Exception {
		Connection connection = getConnection();
		PreparedStatement st = connection.prepareStatement("insert into details values(?,?,?,?)");
		st.setInt(1, person.getId());
		st.setString(2, person.getName());
		st.setLong(3, person.getPhone());
		st.setString(4, person.getEmail());
		st.execute();
		st.close();
		connection.close();
	}

	public void updatePerson(int id, Person person) throws Exception {
		Connection connection = getConnection();
		PreparedStatement st = connection.prepareStatement("update details set name=?,phone=? ,email=? where id=?");
		st.setString(1, person.getName());
		st.setLong(2, person.getPhone());
		st.setString(3, person.getEmail());
		st.setInt(4, id);
		
		st.executeUpdate();
		st.close();
		connection.close();

	}

	public Person getpersonById(int id) throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("select * from details  where id=?");
		st.setInt(1, id);
		ResultSet res = st.executeQuery();

		Person person = new Person();

		while (res.next()) {
			person.setId(res.getInt(1));
			person.setName(res.getString(2));
			person.setPhone(res.getLong(3));
		}
		return person;
	}

	public void getAllPerson() throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("select * from details");
		ResultSet res = st.executeQuery();

		while (res.next()) {
			System.out.println(res.getInt(1) + " " + res.getString(2) + " " + res.getLong(3));
		}

	}

	public void deletPerson(int id) throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("delete from details where id=?");
		st.setInt(1, id);
		st.executeUpdate();
		st.close();
		con.close();

	}

}
