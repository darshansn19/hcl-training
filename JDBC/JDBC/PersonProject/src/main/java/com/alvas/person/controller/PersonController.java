package com.alvas.person.controller;

import java.util.Scanner;

import com.alvas.person.dao.PersonDao;
import com.alvas.person.dto.Person;

public class PersonController {

	public static void main(String[] args) throws Exception {
		boolean control = true;
		PersonDao p = new PersonDao();
		while (control) {
			System.out.println("......enter the choice......");
			System.out.println("1.insertperson");
			System.out.println("2.update the person");
			System.out.println("3.Getperson by id");
			System.out.println("4.Get all person");
			System.out.println("5.Delete the person");
			System.out.println("6.exit");
			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();
			switch (ch) {
			case 1: {
				System.out.println("enter the id,name,phone,email");
				int id = sc.nextInt();
				String name = sc.next();
				long phone = sc.nextLong();
				String email = sc.next();

				Person person = new Person();
				person.setId(id);
				person.setName(name);
				person.setPhone(phone);
				person.setEmail(email);
				p.insertPerson(person);
				System.out.println("successfully inserted\n");
				break;
			}
			case 2: {
				System.out.println("enter the id to be updated");
				int id = sc.nextInt();
				
				System.out.println("enter updated name,phone,mail");
				String name = sc.next();
				long phone = sc.nextLong();
				String email = sc.next();
				
				Person person = new Person();
				person.setName(name);
				person.setPhone(phone);
				person.setEmail(email);
				p.updatePerson(id, person);
				System.out.println("successfully updated\n");
				break;
			}
			case 3: {
				System.out.println("enter the id you want");
				int id = sc.nextInt();
				
				Person person = p.getpersonById(id);
				System.out.println(person.getId() + " " + person.getName() + " " + person.getPhone() + " " + person.getEmail());
				System.out.println("\n");
				break;
			}
			case 4: {
				p.getAllPerson();
				System.out.println("\n");
				break;
			}
			case 5: {
				System.out.println("enter the id to be deleted");
				int id = sc.nextInt();
				p.deletPerson(id);
				System.out.println("successfully deleted\n");
				break;
			}
			case 6: {
				control = false;
				break;
			}
			default: {
				System.out.println("invalid id...please enter again");
			}

			}
		}

	}

}
