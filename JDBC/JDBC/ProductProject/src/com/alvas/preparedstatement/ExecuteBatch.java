package com.alvas.preparedstatement;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExecuteBatch {

	public static void main(String[] args) throws SQLException {
	
		List<Product> product=new ArrayList<>();
		Product p1=new Product();
		p1.setId(11);
		p1.setName("pencil");
		p1.setBrand("nataraj");
		p1.setPrice(10);
		p1.setQuantity(6);
		
		Product p2=new Product();
		p2.setId(12);
		p2.setName("pencil");
		p2.setBrand("nataraj");
		p2.setPrice(10);
		p2.setQuantity(6);
		
		Product p3=new Product();
		p3.setId(13);
		p3.setName("pencil");
		p3.setBrand("nataraj");
		p3.setPrice(10);
		p3.setQuantity(6);
		
		Product p4=new Product();
		p4.setId(14);
		p4.setName("pencil");
		p4.setBrand("nataraj");
		p4.setPrice(10);
		p4.setQuantity(6);
		
		Product p5=new Product();
		p5.setId(15);
		p5.setName("pencil");
		p5.setBrand("nataraj");
		p5.setPrice(10);
		p5.setQuantity(6);
		
		product.add(p1);
		product.add(p2);
		product.add(p3);
		product.add(p4);
		product.add(p5);
		ProductCrud c=new ProductCrud();
		c.batchExecution(product);
		

	}

}
