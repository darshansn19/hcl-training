package com.alvas.preparedstatement;

import java.sql.SQLException;
import java.util.Scanner;

public class SaveProduct {
	public static void main(String[] args) throws SQLException {
		Product p = new Product();
		p.setId(2);
		p.setName("pencil");
		p.setBrand("Nataraj");
		p.setPrice(5);
		p.setQuantity(10);
		ProductCrud pc = new ProductCrud();
		pc.saveProduct(p);

	}
}
