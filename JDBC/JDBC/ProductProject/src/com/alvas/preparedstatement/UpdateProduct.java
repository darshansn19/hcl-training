package com.alvas.preparedstatement;

import java.sql.SQLException;

public class UpdateProduct {
	public static void main(String[] args) {
		ProductCrud pc = new ProductCrud();
		Product p = new Product();
		p.setName("pen");
		p.setBrand("cello");
		p.setPrice(20);
		try {
			System.out.println(pc.updateProduct(1, p));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
