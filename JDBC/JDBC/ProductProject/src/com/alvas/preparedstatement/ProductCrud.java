package com.alvas.preparedstatement;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import com.mysql.cj.jdbc.Driver;

public class ProductCrud {

	public void saveProduct(Product p) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Driver d = new Driver();
			DriverManager.registerDriver(d);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			ps = con.prepareStatement("insert into Product value(?,?,?,?,?)");
			ps.setInt(1, p.getId());
			ps.setString(2, p.getName());
			ps.setString(3, p.getBrand());
			ps.setDouble(4, p.getPrice());
			ps.setInt(5, p.getQuantity());
			ps.execute();
			ps.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.close();
			ps.close();
		}

	}

	public int deleteProduct(int id) throws SQLException {
		Driver d = new Driver();
		try {
			DriverManager.registerDriver(d);
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			PreparedStatement ps = con.prepareStatement("delete from product where id=?");
			ps.setInt(1, id);
			int a = ps.executeUpdate();
			return a;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public int updateProduct(int id, Product p) throws SQLException {
		Driver d = new Driver();
		try {
			DriverManager.registerDriver(d);
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			PreparedStatement ps = con.prepareStatement("update Product set name=?,brand=?,price=? where id=?");
			ps.setInt(4, id);
			ps.setString(1, p.getName());
			ps.setString(2, p.getBrand());
			ps.setDouble(3, p.getPrice());
			int a = ps.executeUpdate();
			return a;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public Product getProductByid(int id) throws SQLException {
		Driver d = new Driver();
		Product p = null;
		try {
			DriverManager.registerDriver(d);
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			PreparedStatement ps = con.prepareStatement("select * from Product where id=?");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				p = new Product();

				p.setId(rs.getInt(1));
				p.setName(rs.getString(2));
				p.setBrand(rs.getString(3));
				p.setPrice(rs.getDouble(4));
				p.setQuantity(rs.getInt(5));

			}
			return p;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;

	}

	public ArrayList<Product> getAllProduct() throws SQLException {
		
		ArrayList<Product> a =new ArrayList<>();
		Driver d = new Driver();
		Connection con = null;
		PreparedStatement ps = null;
		try {
			DriverManager.registerDriver(d);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			ps = con.prepareStatement("select * from   product");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Product p = new Product();
				p.setId(rs.getInt(1));
				p.setName(rs.getString(2));
				p.setBrand(rs.getString(3));
				p.setPrice(rs.getDouble(4));
				p.setQuantity(rs.getInt(5));
				a.add(p);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return a;

	

	}
	public void batchExecution(List<Product> prs) throws SQLException
	{
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Driver d = new Driver();
			DriverManager.registerDriver(d);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_phase2", "TestUser", "root");
			ps = con.prepareStatement("insert into Product value(?,?,?,?,?)");
			for(Product p:prs)
			{
				ps.setInt(1, p.getId());
				ps.setString(2, p.getName());
				ps.setString(3, p.getBrand());
				ps.setDouble(4, p.getPrice());
				ps.setInt(5, p.getQuantity());
				ps.addBatch();
			}
		int[] a=ps.executeBatch();
		for(int i=0;i<a.length;i++)
		{
			System.out.println(a[i]);
		}
		
			ps.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.close();
			ps.close();
		}
	}
}
