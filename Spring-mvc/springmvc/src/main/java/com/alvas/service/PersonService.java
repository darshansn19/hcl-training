package com.alvas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alvas.dao.PersonDao;
import com.alvas.dto.Person;
@Component
public class PersonService {
	@Autowired
	PersonDao personDao;
	@RequestMapping(value="/register")
	public Person savePerson(Person person) {
		return personDao.savePerson(person);
	}
}
