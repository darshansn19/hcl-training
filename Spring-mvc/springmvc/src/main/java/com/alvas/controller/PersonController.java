package com.alvas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alvas.dto.Person;
import com.alvas.service.PersonService;
@Controller
public class PersonController {
	@Autowired
	PersonService personService;

	@RequestMapping("/loadperson")
	public ModelAndView loadPerson() {
		ModelAndView modelAndView = new ModelAndView("saveperson.jsp");
		modelAndView.addObject("person", new Person());
		return modelAndView;
	}
	
	
	@RequestMapping("/saveperson")
		public ModelAndView savePerson(@ModelAttribute Person person)
		{
			Person person2=personService.savePerson(person);
			ModelAndView modelAndView=new ModelAndView("common.jsp");
			if(person2!=null){
				modelAndView.addObject("msg","perosn saved successfully");
				
			}else
			{
				modelAndView.addObject("msg","Failed to save Person");
			}
			return modelAndView;
		}
}
