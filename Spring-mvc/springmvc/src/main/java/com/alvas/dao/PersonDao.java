package com.alvas.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;

import com.alvas.dto.Person;

public class PersonDao {
	@Autowired
	EntityManagerFactory entityManagerFactory;

	public Person savePerson(Person person) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		entityManager.persist(person);
		entityTransaction.commit();
		return entityManager.find(Person.class,person.getId());
	}
}
