package com.alvas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.alvas.dto.Student;

@Component
public class StudentDao {
	@Autowired
	EntityManagerFactory entityManagerFactory;
	EntityManager entityManager;
	EntityTransaction entityTransaction;

	public Student saveStudent(Student student) {
		
		entityManager = entityManagerFactory.createEntityManager();
		entityTransaction = entityManager.getTransaction();

		entityTransaction.begin();
		entityManager.persist(student);
		entityTransaction.commit();
		return student;
	}

	public Student getStudentById(int id) {

		return entityManager.find(Student.class, id);

	}

	public List<Student> getAllStudent() {

		Query query = entityManager.createNamedQuery("select s from Student s");
		return query.getResultList();
	}

	public boolean deleteStudentById(int id) {

		Student student1 = entityManager.find(Student.class, id);
		if (student1 != null) {
			entityTransaction.begin();
			entityManager.remove(student1);
			entityTransaction.commit();
			return true;
		} else
			System.out.println("student not found");
		return false;

	}

	public Student updateStudent(Student student) {
		Student student2 = entityManager.find(Student.class, student.getId());
		if (student2 != null) {
			student.setId(student2.getId());
			entityTransaction.begin();
			entityManager.merge(student);
			entityTransaction.commit();
			return student;
		} else {
			return null;
		}

	}

}
