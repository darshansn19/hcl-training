package com.alvas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alvas.dto.Student;
import com.alvas.service.StudentService;

@Controller
public class StudentController {
	@Autowired
	StudentService studentService;

	@RequestMapping("/loadstudent")
	public ModelAndView loadstudent() {
		ModelAndView modelAndView = new ModelAndView("register.jsp");
		modelAndView.addObject("student", new Student());
		return modelAndView;
	}

	@RequestMapping("/registerstudent")
	public ModelAndView registerstudent(@ModelAttribute Student student) {

		Student student1=studentService.saveStudent(student);
		ModelAndView andView=new ModelAndView("common.jsp");
		return andView;
	}
}
