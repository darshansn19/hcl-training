package com.alvas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alvas.dao.StudentDao;
import com.alvas.dto.Student;

@Component
public class StudentService {
	@Autowired
	StudentDao dao;

	public Student saveStudent(Student student) {
		return dao.saveStudent(student);
	}

	public Student getStudentById(int id) {
		return dao.getStudentById(id);
	}

	public List<Student> getAllStudent() {
		return dao.getAllStudent();
	}
	
	 public boolean deleteStudentById(int id){
		 return dao.deleteStudentById(id);
	 }
	 public Student updateStudent(Student student){
		 return dao.updateStudent(student);
	 }
}
