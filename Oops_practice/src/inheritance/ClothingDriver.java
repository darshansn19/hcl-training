package inheritance;

public class ClothingDriver {

	public static void main(String[] args) {
		 Clothing cloth=new Clothing(1, "good", 'G', 1000);
		 Shirt shirt=new Shirt(2, "nice ", 'P', 2000, 'u');
		 
		 System.out.println("parent class");
		 cloth.display();
		 System.out.println();
		 System.out.println("sub class");
		shirt.display();
		 

	}

}
