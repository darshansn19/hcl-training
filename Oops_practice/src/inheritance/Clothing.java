package inheritance;

public class Clothing {
	public int id;
	public String des;
	public char color;
	public double price;
	
	public Clothing(int id, String des, char color, double price) {
		super();
		this.id = id;
		this.des = des;
		this.color = color;
		this.price = price;
	}
	public void display()
	{
		System.out.print("id:"+id+" "+"des:"+des+" "+"color:"+color+" "+"price:"+price);
	}
}
