package inheritance;

public class Employe {
	
	private int ssn;
	private String name;
	private int age;
	private double salary;
	private long phono;
	
	public Employe(int ssn, String name, int age, double salary, long phono) {
		super();
		this.ssn = ssn;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.phono = phono;
	}

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public long getPhono() {
		return phono;
	}

	public void setPhono(long phono) {
		this.phono = phono;
	}
	
//	public void EmployeeDetails()
//	{
//		System.out.println("ssn:"+getSsn()+"\n name: "+ getName()+"\n age: "+getAge()+"\n salary:"+getSalary()+"\n phone: "+getPhono());
//	}
}

