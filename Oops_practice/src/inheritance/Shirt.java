package inheritance;

public class Shirt extends Clothing {
	private char fit='u';

	public char getFit() {
		return fit;
	}

	public void setFit(char fit) {
		this.fit = fit;
	}


	public Shirt(int id, String des, char color, double price, char fit) {
		super(id, des, color, price);
		this.fit = fit;
	}
	
	@Override
	public void display() {
		super.display();
		System.out.print(" fit:"+fit);
	}

}
