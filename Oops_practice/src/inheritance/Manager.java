package inheritance;

public class Manager extends Employe {
	private String dept;

	public Manager(int ssn, String name, int age, double salary, long phono,String dept) {
		super(ssn,name,age,salary,phono);
		this.dept = dept;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}


	

	public void employeDetails(){
		System.out.println("ssn:"+getSsn()+"\n name: "+ getName()+"\n age: "+getAge()+"\n salary:"+getSalary()+"\n phone: "+getPhono()+"\ndept");

	}
	
	
	
	
	

}
