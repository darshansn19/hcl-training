package encapsulation;

public class ConstantExample {

	public static void main(String[] args) {
		
		ConstantExamples constant= new ConstantExamples();
		System.out.println(constant.a);
		System.out.println(constant.place);
		System.out.println(ConstantExamples.price);    			//if the members defined are non static access them by instantiating .
														//if the members are static access them with class name
		

	}

}
