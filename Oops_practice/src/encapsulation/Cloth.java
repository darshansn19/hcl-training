package encapsulation;

public class Cloth {
	private  int id=10;
	private  String color="green";
	private  double price=100;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void display()
	{
		System.out.println("id="+getId()+"\ncolor="+getColor()+"\nprice="+getPrice());
	}
}
