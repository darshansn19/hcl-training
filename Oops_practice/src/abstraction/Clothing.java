package abstraction;

public  abstract class Clothing {
	private int id;
	
	protected  abstract void display();
	protected abstract int getprice();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
    