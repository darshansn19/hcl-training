package com.hcl.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.xdevapi.Result;

@WebServlet("/users")
public class ChooseUser extends HttpServlet{
	
	public void service(HttpServletRequest req,HttpServletResponse res) throws IOException
	{
		System.out.println(req.getParameter("languages"));
		String lang=req.getParameter("languages");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			java.sql.Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root");
			System.out.println("conection estblished");
			ResultSet rd=null;
			PreparedStatement st=null;
			if(req.getParameter("languages").equals("any"))
			{
				 st=con.prepareStatement("select r.firstname,r.lastname,r.phone,r.email,l.language from register r,languages l where r.email=l.email");
				rd=st.executeQuery();
			}
			else{
			 st=con.prepareStatement("select r.firstname,r.lastname,r.phone,r.email,l.language from register r,languages l where r.email=l.email and l.language=?");
			st.setString(1, lang);
			 rd=st.executeQuery();
			}
		
			
			PrintWriter out=res.getWriter();
			while(rd.next())
			{
				
				out.println("first name:"+rd.getString("firstname")+" last name:"+rd.getString("lastname")+" phone:"+rd.getString("phone")+"emailId: "+ rd.getString("email")+" language:"+rd.getString("language"));
				
				
			}
			
			st.close();
			con.close();
			
			
		
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
	
		
	}

}
